defmodule Movement.App do
  @moduledoc false

  use Application

  def start( _, _) do

    import Supervisor.Spec

    opts = [strategy: :one_for_one, name: Movement.Supervisor]

    children = [
      worker(Movement.Server, [])
    ]

    Supervisor.start_link(children, opts)

  end

end
