defmodule Movement.ArduinoConnect do
  @moduledoc false

  alias Nerves.UART

  alias Movement.MovementStruct

  require Logger

  @arduino_name "ttyACM0"

  @baud_rate 9600

  def start() do
    {:ok, pid} = UART.start_link

    Logger.debug("running scripted run")

    :timer.sleep(5000)

    UART.open(pid, @arduino_name, speed: @baud_rate, active: false)

    wait_for_calibration pid

    {:ok, pid}
  end

  def move_up(pid, speed) do
    Logger.debug "moving up"
    move_up = %MovementStruct{
      leftSpeed: speed,
      rightSpeed: speed,
    }
    execute_movement(pid, move_up)
  end

  def move_left(pid, speed) do
    Logger.debug "moving left"
    move_left = %MovementStruct{
      leftSpeed: -speed,
      rightSpeed: speed,
    }
    execute_movement(pid, move_left)
  end

  def move_right(pid, speed) do
    Logger.debug "moving right"
    move_right = %MovementStruct{
      leftSpeed: speed,
      rightSpeed: -speed,
    }
    execute_movement(pid, move_right)
  end

  def move_back(pid, speed) do
    Logger.debug "moving back"
    move_back = %MovementStruct{
      leftSpeed: -speed,
      rightSpeed: -speed,
    }
    execute_movement(pid, move_back)
  end

  defp execute_movement(pid, movement_map) do
    Logger.debug "executing movement"
    UART.write(pid, Poison.encode!(movement_map))
    {:ok, pid}
  end

  defp wait_for_calibration(pid) do

    Logger.debug("Attemping calibration")

    calibration_input = %{:calibrationComplete => true}

    :timer.sleep(6000)

    {:ok, arduino_message} = UART.read(pid, 600)

    Logger.debug("Arduino Message: " <> arduino_message)

    cond do

      arduino_message =~ "leftSpeed" ->
        Logger.debug("Calibration Complete")
        {:ok, pid}

      true ->
        Logger.debug("Calibration Failed. Retrying...")
        UART.write(pid, Poison.encode!(calibration_input))
        wait_for_calibration(pid)

    end

  end


end
