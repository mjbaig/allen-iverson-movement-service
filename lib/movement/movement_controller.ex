defmodule MovementController do
  @moduledoc false

  def move_up(speed) do
    GenServer.cast(:arduino_movement, {:move_up, speed})
  end

  def move_left(speed) do
    GenServer.cast(:arduino_movement, {:move_up, speed})
  end

  def move_right(speed) do
    GenServer.cast(:arduino_movement, {:move_up, speed})
  end

  def move_down(speed) do
    GenServer.cast(:arduino_movement, {:move_up, speed})
  end

end
