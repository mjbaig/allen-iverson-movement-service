defmodule Movement.Server do
  @moduledoc """
  Documentation for Movement.
  """

  alias Movement.ArduinoConnect

  use GenServer

  require Logger

  def start_link do
    Logger.debug("Starting ArduinoMovement server")
    GenServer.start_link(__MODULE__, [], name: :arduino_movement)
  end

  def init(_) do
    ArduinoConnect.start()
  end

  def handle_cast({:move_up, speed}, movement_pid) do
    Logger.debug("Executing gen server call to move up")
    {:ok, _} =  ArduinoConnect.move_up(movement_pid, speed)
    {:reply, :ok, movement_pid}
  end

  def handle_cast({:move_left, speed}, movement_pid) do
    Logger.debug("Executing gen server call to move left")
    {:ok, _} = ArduinoConnect.move_left(movement_pid, speed)
    {:reply, :ok, movement_pid}
  end

  def handle_cast({:move_right, speed}, movement_pid) do
    Logger.debug("Executing gen server call to move right")
    {:ok, _} = ArduinoConnect.move_right(movement_pid, speed)
    {:reply, :ok, movement_pid}
  end

  def handle_cast({:move_back, speed}, movement_pid) do
    Logger.debug("Executing gen server call to move back")
    {:ok, _} = ArduinoConnect.move_back(movement_pid, speed)
    {:reply, :ok, movement_pid}
  end

end
