defmodule Movement.MovementStruct do
  @moduledoc false

  defstruct(
    setMotorSpeed: true,
    leftSpeed: 0,
    rightSpeed: 0
  )

end
