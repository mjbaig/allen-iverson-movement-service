defmodule Movement.MixProject do
  use Mix.Project

  def project do
    [
      app: :movement,
      version: "0.1.0",
      elixir: "~> 1.7",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger],
      mod: {Movement.App, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:elixir_ale, "~> 1.0.0"},
      {:nerves_uart, "~> 1.2"},
      {:poison, "~> 3.1"},
      {:mock, "~> 0.3.0", only: :test}
    ]
  end

  defp package do
    [
      maintainers: ["Maz Baig"],
      licenses: ["Apache 2.0"],
      links: %{"Github" => "https://gitlab.com/mjbaig/allen-iverson-movement-service"}
    ]
  end

end
